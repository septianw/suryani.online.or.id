<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true); //Added by WP-Cache Manager
define('DB_NAME', getenv('DBNAME'));

/** MySQL database username */
define('DB_USER', getenv('DBUSER'));

/** MySQL database password */
define('DB_PASSWORD', getenv('DBPASS'));

/** MySQL hostname */
define('DB_HOST', getenv('DBHOST'));

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'b^PG&1ug!F2*`5OzVw3yzjz6_x^t!GnjGx6GuK;!43K?@U371!a(zBx6vQx:vPg)');
define('SECURE_AUTH_KEY',  'L_gF#7(DDeP5CVmfoGIV^^VoQAv?DO^@4VQfr4FudPXD6h:N05Pk%z@|csIE(L`)');
define('LOGGED_IN_KEY',    'MNj3l1FNsKnpE`I$rUl7KR^OoUa~Io#nLmYvZ0yuS^VrYqZ0LO!nV^x|*W~:|^$/');
define('NONCE_KEY',        'c/p|b4a?9W_iim0"0Iosqi_Q@qIY0b;sMTTo~8AYmOe2fSI7ldOhg_3sYM0P!wBm');
define('AUTH_SALT',        't;yjNgoKdQ9eK|)@^z@P!0oU^#p(u5Zqw@IAgeR#jhM)TS7UC|djOgR7;QU/FvA`');
define('SECURE_AUTH_SALT', 'j&*caK2vjxc@oIx~m;Gq5*/8@Kuy57K#k%Vwjy0;_#7ROOL8nD$*M:Rd7PrW1UUv');
define('LOGGED_IN_SALT',   'K4c`akDMV2N:;HFoY_&R+_7cVK4/x66xhLk24%Xqb(_91mL)g@TjpD5ctmCpajKp');
define('NONCE_SALT',       '`nTOLPe:"QdcKMUgIXYVsxVU)qZ`O%bbiS+Fh0s7b&+c0AZTLe$RrUpROl0Lnu(E');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_894wkx_';

/**
 * Limits total Post Revisions saved per Post/Page.
 * Change or comment this line out if you would like to increase or remove the limit.
 */
define('WP_POST_REVISIONS',  10);

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

