jQuery(document).ready(function($) {
    $(function() {
        $('img').lazyload({
            effect : 'fadeIn'
        });

        var high = 0;
        var blocks = $('.bottom');
        blocks.each(function(){
            var height = $(this).height();
            if (high <= height) high = height;
        });
        blocks.each(function(){
            $(this).height(high);
        });
    });
});